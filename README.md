# SharePoint Manager

This repository uses the Sharepoint API for Python to facilitate the upload and download of directories to and from Sharepoint.

First, make sure to install the required package modules:

    $ python3 -m pip install -r utilities/requirements.txt

Then, edit the utilities/settings.json file to include the Sharepoint URL site and the directories you want to upload/download, both from your
local PC and from your Sharepoint site.

Finally, to download the directory specified in settings.json:

    $ python3 sharepoint_downloader.py

And to upload the directory specified in settings.json:

    $ python3 sharepoint_uploader.py
    
**Update:** Now you can upload single files. Specify the file path in utilities/settings.json, and run:

    $ python3 upload_file.py
    
Each uploaded will be split in multiple parts if it's heavier than 240MB. All parts will be grouped under a directory named `<filename>_parts`. 
If `sharepoint_downloader.py` is used to download a folder containing the parts of a file previously compressed by this application, the file will be automatically
extracted and placed in the desired location, while getting rid of all the compressed downloaded parts. 
    
## Docker usage

To use with Docker, reproduce the following steps

1. Install Docker (https://docs.docker.com/engine/install/ubuntu/).
2. (Only if you want to use the Uploader) Open the file `Dockerfile`, and in the last line change `sharepoint_downloader.py` for `sharepoint_uploader.py`.
3. `cd` to the `scharepoint_manager/scripts` directory and run:  
    `$ sudo docker build -t sharepoint_manager .`
4. Now run the docker container:  
    `$ sudo docker run --rm -it -v <LOCAL_DIR>:<LOCAL_DIR> sharepoint_manager`  
    where `<LOCAL_DIR>` refers to the download/upload directory you have set in settings.json.

Remember to run points `2.` and `3.` every time you change any file (such as the directories in `settings.json` o the `Dockerfile`).