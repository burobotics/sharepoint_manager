#!/usr/bin/env python3

import os
import json
import subprocess as sp

with open("./utilities/settings.json","r") as file:
    config = json.load(file)

download_local_dir = config["download_settings"]["local_path"]
download_remote_dir = config["download_settings"]["remote_path"]
upload_local_dir = config["upload_settings"]["local_path"]
upload_remote_dir = config["upload_settings"]["remote_path"]

app_dir = os.getcwd()

sp.call('docker run -it --rm -v "{}":/usr/settings -v "{}":/usr/download_local_dir -v "{}":/usr/upload_local_dir mmitjans9/sharepoint-manager'.format(
    app_dir,
    download_local_dir,
    upload_local_dir
),shell=True,executable="bash")