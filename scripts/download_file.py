#!/usr/bin/env python

from sharepoint_downloader import *

if __name__ == "__main__":

    '''Download files from SharePoint'''
    url = config["sharepoint_settings"]["url"]
    sharepoint_parser = SharePointDownloader()
    sharepoint_parser.add_user(url)

    sharepoint_parser.overwrite_all = True

    remote_path = config["download_settings"]["remote_path"]
    local_path = config["download_settings"]["local_path"]
    # if not os.path.exists(os.path.dirname(local_path)):
    #     os.mkdir(os.path.dirname(local_path))

    # Capture keyboard interrupts:
    signal.signal(signal.SIGINT,sharepoint_parser.signal_shutdown)

    '''Full extraction of a session'''
    sharepoint_parser.download_file(local_path,remote_path)