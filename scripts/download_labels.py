#!/usr/bin/env python3

from sharepoint_downloader import *

with open(os.environ["HOME"] + "/catkin_ws/src/movement_assessment/sharepoint_manager/scripts/utilities/settings.json",'r') as f:
    config = json.load(f)


if __name__ == "__main__":

    '''Download files from SharePoint'''
    url = config["sharepoint_settings"]["url"]
    base_url = url.split('.com')[-1]
    sp_downloader = SharePointDownloader(url)
    
    home_dir = os.environ["HOME"]
    bag_folder = os.path.join(home_dir,'catkin_ws/bags/NRL_OFFICIAL')

    for filepath in glob.glob(bag_folder + "/**",recursive=True):
        if os.path.isdir(filepath) and '_Data' in os.path.basename(filepath) and 'skip' not in filepath:
            local_path = filepath[:-4] + 'Label'
            remote_path = base_url + '/Shared Documents/General/NRL_OFFICIAL' + local_path.split('NRL_OFFICIAL')[1]
            # print(local_path,remote_path)
            print(remote_path)
            sp_downloader.download_folder(local_path,remote_path)


