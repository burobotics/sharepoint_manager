#!/usr/bin/env python3

import subprocess as sp
import threading
import os
from termcolor import colored

def extract_folder(bag_dir):
    '''Folder extractor'''
    
    download_path = os.path.dirname(bag_dir)
    filename = os.path.basename(download_path.split('_parts')[0])
    
    print("Extracting {}...".format(filename))

    extracted_filename = os.path.join(os.path.dirname(download_path),filename) 

    # Check if extracted file exists; it it does, remove it before extracting:
    if os.path.isfile(extracted_filename):
        os.remove(extracted_filename)

    result = sp.call('7z x "{}.7z.*" -o"{}" -aoa &> /dev/null'.format(os.path.join(download_path,filename),\
            os.path.dirname(download_path)),shell=True,executable='bash')

    if result:
        print(colored("[ERROR] Couldn't extract the file {}, error code: {}.".format(filename,result),"red"))
        # Store the paths of the file in a list to re-try them at the end:
        # Remove the file so that it doesn't confuse and it doesn't bother future downloads:
        if os.path.isfile(extracted_filename):
            os.remove(extracted_filename)
        return

        
    # Now remove the 7z parts:
    result = sp.call('rm "{}.7z"*'.format(os.path.join(download_path,filename)),shell=True,executable='bash')
    if result:
        print(colored("[ERROR] Couldn't remove .7z parts, error code: {}".format(result),"red"))
    elif not os.listdir(download_path):
        os.rmdir(download_path)

    print(colored("[OK] File {} extracted successfully.".format(filename),"green"))


if __name__ == "__main__":
    # Walking through all the files and folder, extract them

    threads = list()

    root_dir = os.environ["HOME"] + '/OTHER_HOME_DATA/HOME_DATA'

    for root,dirnames,filenames in os.walk(root_dir):

        if filenames and '.7z' in filenames[0] and "_parts" in root:
            # x = threading.Thread(target=extract_folder,args=(os.path.join(root,filenames[0]),))
            # threads.append(x)
            # x.start()
            continue

        elif not dirnames and '_parts' not in root and "_bags" in os.path.basename(root) and 'DONE.txt' not in filenames:
            # Print which bags are ready to review
            print("Bag {} is ready to review.".format(os.path.basename(root)))

            ## If this bag is already extracted, add a token
            # print("Adding token to {}".format(os.path.basename(root)))
            # sp.call('touch {}/DONE.txt'.format(root),executable="bash",shell=True)

    print("Waiting for remaining bags to extract...")

    for thread in threads:
        thread.join()
