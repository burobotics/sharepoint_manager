#!/usr/bin/env python

import rospy
from getpass import getpass

import signal
import json

from termcolor import cprint
import os
from sharepoint_manager_pkg.msg import *
import actionlib

class SharePointClient(object):

    def __init__(self):
        self.sharepoint_client = None

    def signal_shutdown(self,sig,num):
        # First, cancel the download
        if self.sharepoint_client is not None:
            print("Cancelling the sharepoint download.")
            self.sharepoint_client.cancel_all_goals()

            # Then, tell the server to remove unfinished downloads
            cancel_sharepoint_client = actionlib.SimpleActionClient('cancel_sharepoint',CancelSharePointAction)
            goal = CancelSharePointGoal()
            cancel_sharepoint_client.wait_for_server(timeout=rospy.Duration(1))
            cancel_sharepoint_client.send_goal(goal)

        rospy.signal_shutdown("Clean shutdown")

    def call_sharepoint_action(self,url,username,password,local_path,remote_path):
        print("Waiting for Sharepoint action server...")
        successful_call = False
        try:
            while not successful_call and not rospy.is_shutdown():
                self.sharepoint_client = actionlib.SimpleActionClient('sharepoint_action', SharePointAction)
                self.sharepoint_client.wait_for_server()
                goal = SharePointGoal(url=url,username=username,password=password,local_path=local_path,remote_path=remote_path)
                self.sharepoint_client.send_goal(goal)
                self.sharepoint_client.wait_for_result()
                successful_call = self.sharepoint_client.get_result().result
                if not successful_call:
                    print("Wrong credentials, try again...")
                    username = input("Username: ")
                    password = getpass()
        except Exception as e:
            cprint("[ERROR] Couldn't call SharePoint manager!","red")
            return -1

        if not rospy.is_shutdown():
            print("Downloading file... Press CTRL+C to cancel download.")
        rospy.spin()
        
        print("Download suceeded!")
        
if __name__ == "__main__":

    with open('./utilities/settings.json','r') as f:
        config = json.load(f)

    rospy.init_node("sharepoint_manager_node",anonymous=False)

    sharepoint_client = SharePointClient()

    signal.signal(signal.SIGINT,sharepoint_client.signal_shutdown)

    username = input("Username: ")
    password = getpass()

    remote_path = config["download_settings"]["remote_path"]
    # download_path = os.path.join('/usr/download_local_dir',os.path.basename(remote_path))
    local_path = os.path.join(config["download_settings"]["local_path"],os.path.basename(remote_path))
    if not os.path.exists(os.path.dirname(local_path)):
        os.mkdir(os.path.dirname(local_path))

    url = config["sharepoint_settings"]["url"]

    sharepoint_client.call_sharepoint_action(url,username,password,local_path,remote_path)