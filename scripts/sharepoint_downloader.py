#!/usr/bin/env python3

import glob
import signal
import subprocess as sp
import threading

from sharepoint_manager import *

"""
To run the "downloader": 
$ rosrun sharepoint_manager sharepoint_downloader.py
"""


class SharePointDownloader(SharePointManager):
    def __init__(self, url, username=None, password=None):
        super().__init__(url, username, password)
        self.threads = []

        self.bad_downloads = []

    def extract_folder(self, download_path, remote_path):
        # Once all parts are downloaded, extract the rosbag:
        filename = os.path.basename(download_path.split("_parts")[0])
        cprint(f"Extracting {filename} on a separate thread...", "cyan")

        extracted_filename = os.path.join(os.path.dirname(download_path), filename)

        # Check if extracted file exists; it it does, remove it before extracting:
        if os.path.isfile(extracted_filename):
            os.remove(extracted_filename)

        self.extracting_files.append(extracted_filename)

        result = sp.call(
            f'7z x "{os.path.join(download_path, filename)}.7z.*" -o"{os.path.dirname(download_path)}" -aoa &> /dev/null',
            shell=True,
            executable="bash",
        )

        # Remove the file from the list after it's extracted, regardless of whether it was properly extracted or not.
        self.extracting_files.pop(self.extracting_files.index(extracted_filename))

        if result:
            cprint(
                f"[ERROR] Couldn't extract the file {filename}, error code: {result}.",
                "red",
            )
            # Store the paths of the file in a list to re-try them at the end:
            self.bad_downloads.append((filename, download_path, remote_path))
            # Remove the file so that it doesn't confuse and it doesn't bother future downloads:
            if os.path.isfile(extracted_filename):
                os.remove(extracted_filename)
            return

        if result := sp.call(
            f'rm "{os.path.join(download_path, filename)}.7z"*',
            shell=True,
            executable="bash",
        ):
            cprint(f"[ERROR] Couldn't remove .7z parts, error code: {result}", "red")

        cprint(f"[OK] File {filename} extracted successfully.", "green")

        # Finally, remove the directory if empty:
        if os.path.isdir(download_path) and not os.listdir(download_path):
            os.rmdir(download_path)

    def download_folder(self, download_path, remote_path):

        if "SKIP" in os.path.basename(remote_path):
            print(f"Skipping folder '{os.path.basename(remote_path)}'.")
            return

        folders = self.retrieve_folders(os.path.join(self.base_url, remote_path))
        files = self.retrieve_files(os.path.join(self.base_url, remote_path))

        for folder in folders:
            # Download all folders
            rpath = os.path.join(self.base_url, remote_path, folder)
            new_download_path = os.path.join(download_path, folder)
            self.download_folder(new_download_path, rpath)

        ############## Logic to skip certain directories -- bound to change depending on application ################
        # skip_dir = True
        # sessions_list = ['0.019','0.039','0.01','0.014','0.029']
        # for session in sessions_list:
        #     if session+'_bags' in remote_path and ("(1)_parts" in os.path.basename(remote_path) or "(2)_parts" in os.path.basename(remote_path)):
        #         skip_dir = False
        #         break

        # if skip_dir:
        #     print("Skipping folder '{}'.".format(os.path.basename(remote_path)))
        #     return
        ##################### END OF SKIP DIRECTORIES LOGIC ########################################################

        if not os.path.exists(download_path):
            os.makedirs(download_path)

        for file in files:
            # Now download any files the folder might have:
            if ".hash" in file[-5:]:
                # We'll download the hash when we download its corresponding file:
                continue
            rpath = os.path.join(self.base_url, remote_path, file)
            filename = os.path.basename(rpath)
            potential_extracted_file_dir = os.path.join(os.path.dirname(download_path), filename).split(".7z")[0]
            if (
                os.path.basename(potential_extracted_file_dir) not in os.listdir(os.path.dirname(download_path))
                or ".7z" not in filename
            ):
                # If the file is a .7z file, only download it if the name of the file is not in the directory above
                # (i.e., extracted), or if the file simply isn't a .7z file.
                # If the file exists and we don't want it overwritten, the self.download_file() function will take care of that
                start_time = time.time()
                downloaded_properly = False
                retries = 0
                while not downloaded_properly:
                    # Keep downloading until the local hash matches the remote hash
                    self.download_file(download_path, rpath)

                    # Now downlaod it's hashfile and do a checksum. If checksum fails, re-download previous file:
                    hashfile_path = f"{rpath}.hash"
                    hashfile_name = os.path.basename(hashfile_path)
                    if hashfile_name in files:
                        self.download_file(download_path, hashfile_path)
                        with open(os.path.join(download_path, hashfile_name), "r") as f:
                            remote_hash = f.read()
                        local_hash = self.md5(os.path.join(download_path, filename))
                        if remote_hash != local_hash and retries < self.MAX_CONNECTION_RETRIES:
                            cprint(
                                "[WARN] Checksum failed, file not downloaded properly. Re-trying download...", "yellow"
                            )
                            print("Remote hash:", remote_hash)
                            print("Local hash:", local_hash)
                            os.remove(os.path.join(download_path, file))
                            # Remove the hash file since we don't need it anymore
                            os.remove(os.path.join(download_path, os.path.basename(hashfile_path)))
                            retries += 1
                            continue
                        elif retries == self.MAX_CONNECTION_RETRIES:
                            cprint(
                                "[ERROR] Remote md5 is different than downloaded md5! Keeping the last retry, but check whether the file is corrupt.",
                                "red",
                            )
                        # Remove the hash file since we don't need it anymore
                        os.remove(os.path.join(download_path, os.path.basename(hashfile_path)))

                    downloaded_properly = True
                print(f"File download took {time.time() - start_time:.2f}s.")
            else:
                print(f"File {filename} has already been extracted. Skipping...")
                break

        # Check the files the folder contains. If there are .7z, extract them:
        files_in_folder = ",".join(glob.glob(os.path.join(download_path, "*")))
        if ".7z" in files_in_folder:
            """Each extraction can run on a separate Thread!"""
            x = threading.Thread(target=self.extract_folder, args=(download_path, remote_path))
            self.threads.append(x)
            x.start()

        # Remove the directory if empty. This can happen here if all files in a directory are .7z which had already been extracted.
        if os.path.isdir(download_path) and not os.listdir(download_path):
            os.rmdir(download_path)

    def main_downloader(self, download_path, remote_path):

        if not self.correct_credentials:
            cprint("[WARN] Credentials not stored. Call .add_user(url,username,password) before", "yellow")
            return

        self.download_folder(download_path, remote_path)

        print("Waiting for remaining folders to extract...")
        for thread in self.threads:
            thread.join()

        if self.bad_downloads:
            self.overwrite_all = True
            cprint("Re-downloading corrupt files. Go grab a coffee, it will take a while...", "yellow")
            while self.bad_downloads:
                cprint(
                    f"These files weren't downloaded properly:\n{list(zip(*self.bad_downloads))[0]}",
                    "yellow",
                )
                for _ in range(len(self.bad_downloads)):
                    bad_download = self.bad_downloads.pop(0)
                    self.download_folder(bad_download[1], bad_download[2])
                print("Waiting for remaining folders to extract...")
                for thread in self.threads:
                    thread.join()

        print("All files downloaded and extracted successfully.")


if __name__ == "__main__":

    """Download files from SharePoint"""
    url = config["sharepoint_settings"]["url"]
    sharepoint_parser = SharePointDownloader(url)
    sharepoint_parser.add_user()

    sharepoint_parser.skip_all = True

    remote_path = config["download_settings"]["remote_path"]
    # download_path = os.path.join('/usr/download_local_dir',os.path.basename(remote_path))
    local_path = os.path.join(config["download_settings"]["local_path"], os.path.basename(remote_path))
    if not os.path.exists(os.path.dirname(local_path)):
        os.mkdir(os.path.dirname(local_path))

    # Capture keyboard interrupts:
    signal.signal(signal.SIGINT, sharepoint_parser.signal_shutdown)

    """Full extraction of a session"""
    sharepoint_parser.main_downloader(local_path, remote_path)
