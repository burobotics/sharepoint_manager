#!/usr/bin/env python3

"""
INSTRUCTIONS
--------------
Install the required packages: 
$ python3 -m pip install -r utilities/requirements.txt

This script downloads a full home session, extracts the parts into each individual ROSbag, and then cleans
the .7z parts.
"""

import faulthandler
import hashlib
import json
import os
import shutil

# from pynput import keyboard
import signal
import time
from getpass import getpass

import requests
from office365.runtime.auth.user_credential import UserCredential
from office365.sharepoint.client_context import ClientContext
from termcolor import cprint

# from hanging_threads import start_monitoring
# start_monitoring(seconds_frozen=60,test_interval=1000)

faulthandler.enable()

home_dir = os.environ["HOME"]
try:
    with open("./utilities/settings.json", "r") as file:
        config = json.load(file)
except:
    cprint("[WARN] Couldn't find the settings.json file.\nMake sure to load this file in your application.", "yellow")


class SharePointManager(object):
    def __init__(self, url, username=None, password=None):

        self.url = url
        self.base_url = url.split(".com")[-1]

        if username is not None and password is not None and url is not None:
            self.add_user(username, password)

        # This timeout makes the requests package under the hood of the Officie365 package raise an exception if it can't connect to the sever
        # after X seconds. This timeout is vital to prevent the system from hanging indefinitely.
        # It's also the reason why I had to fork the official Office365 pip package into a custom one, since it didn't have this feature implemented.
        self._requests_connection_timeout = 10
        self.MAX_CONNECTION_RETRIES = 100

        self.current_download = None
        self.current_upload = None
        self.current_compressed_file = None
        self.overwrite_all = False
        self.skip_all = False
        self.extracting_files = []

        self.segfault = False
        self.shutdown = False

        self.correct_credentials = False

        # Capture core dumps (due to segmentation faults and aborts):
        signal.signal(signal.SIGSEGV, self.segfault_handler)
        signal.signal(signal.SIGABRT, self.segfault_handler)

        # Keyboard listener to pause downloads:
        # self.keyboard_listener = keyboard.Listener(on_press=self.press_callback)

    def add_user(self, username=None, password=None):

        correct_credentials = False
        while not correct_credentials:

            # while not correct_credentials:
            if username is None or password is None:
                username = input("Username: ")
                password = getpass()

            # The SharePoint client:
            self.ctx = ClientContext(self.url).with_credentials(UserCredential(username, password))
            try:
                return self._check_connection(username, password)
            except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
                cprint("[WARN] Connection timed out. Do you have internet connection?", "yellow")
            except Exception as e:
                cprint("Either wrong credentials, or the API is down. Try again.", "yellow")

            username, password = None, None

        return 0

    # TODO Rename this here and in `add_user`
    def _check_connection(self, username, password):
        print("Authenticating...")
        web = self.ctx.web.get().execute_query(timeout_=self._requests_connection_timeout)
        print("Site name: {0}".format(web.properties["Title"]))
        self.correct_credentials = True
        self.username = username
        self.password = password
        return 1

    def segfault_handler(self, framenum, sig):
        cprint("[WARN] Core dumped! Retrying download...")
        self.segfault = True

    # def press_callback(self,key):
    #     if key == keyboard.Key.space:
    #         # Pause the downlaod
    #         pass

    def success_callback(self, res):
        pass
        # print("Successful download.")

    def signal_shutdown(self, sig=None, num=None):

        # Necessary attribute in case we're in the middle of an upload
        self.shutdown = True

        if self.current_download is not None:
            os.remove(self.current_download)
            print("Removed incomplete download.")

        if self.current_compressed_file is not None:
            shutil.rmtree(self.current_compressed_file)
            print("Removed .7z files from unfinished compression.")

        for file in self.extracting_files:
            if os.path.isfile(file):
                os.remove(file)

        if self.current_upload is None:
            # If I'm not uploading anything, quit the application right away
            print("Quitting application.")
            cprint("[SUCCESS] You may unplug the hard drives now.", "green")
            os._exit(0)
        else:
            # ... but if I am uploading something, I don't want to quit in the middle of an upload.
            # So we let the upload_folder() function shut down on its own with the
            # self.shutdown attribute.
            cprint("WAIT! Finishing uploading one last file...", "yellow")

    def md5(self, fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def sha256(self, fname):
        hash_sha256 = hashlib.sha256()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_sha256.update(chunk)
        return hash_sha256.hexdigest()

    def upload_file(self, local_path, target_path):
        """Uploads local file to the target_path in Sharepoint.
        target_path needs to start with /sites/... and NOT include the filename"""

        with open(local_path, "rb") as file:
            file_content = file.read()

        # File name of the uploading file:
        filename = os.path.basename(local_path)

        # Check if the file exists on Sharepoint. If it does, ask if we want to overwrite
        folder_files = self.retrieve_files(target_path)
        overwrite = False
        if filename in folder_files and not self.overwrite_all:
            if self.skip_all:
                print(f"File '{filename}' exists, skipping.")
                return
            while not overwrite:
                answer = input("File exists. Overwrite? [Y]es, [N]o, [A]ll, [S]kip all: ")
                if answer == "N":
                    print(f"Not overwriting '{filename}'")
                    return
                elif answer == "Y":
                    overwrite = True
                elif answer == "A":
                    print(f"Overwriting '{filename}' and all future files.")
                    self.overwrite_all = True
                    overwrite = True
                elif answer == "S":
                    print(f"Skipping '{filename}' and all following existing files.")
                    self.skip_all = True
                    return
                else:
                    print("Wrong answer, try again.")

        connection_retries = 0
        while connection_retries < self.MAX_CONNECTION_RETRIES:
            try:
                target_folder = self.ctx.web.get_folder_by_server_relative_url(target_path)
                self.current_upload = os.path.join(target_path, filename)
                target_file = target_folder.upload_file(filename, file_content).execute_query(
                    timeout_=self._requests_connection_timeout
                )
                self.current_upload = None
                print("[OK] File '{}' uploaded successfully to: {}.".format(filename, target_file.serverRelativeUrl))
                return
            except Exception:
                cprint("[WARN] Connection closed. Trying to reconnect...", "yellow")
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))
                connection_retries += 1
                time.sleep(5)

        cprint(f"[ERROR] Couldn't upload file {filename} to {target_path}.", "red")
        raise ConnectionError

    def retrieve_folders(self, rpath):
        """rpath is the relative path, starting with /sites/..."""
        connection_retries = 0
        while connection_retries < self.MAX_CONNECTION_RETRIES:
            try:
                folder = self.ctx.web.get_folder_by_server_relative_url(rpath)
                fold_names = []
                sub_folders = folder.folders  # Replace files with folders for getting list of folders
                self.ctx.load(sub_folders)
                self.ctx.execute_query(timeout_=self._requests_connection_timeout)

                for s_folder in sub_folders:
                    fold_names.append(os.path.join(s_folder.properties["Name"]))

                return fold_names

            except Exception as e:
                cprint("[WARN] Problem printing out library contents. Retrying...", "yellow")
                connection_retries += 1
                time.sleep(5)
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))

        cprint("[ERROR] Can't connect to the server.", "red")
        raise ConnectionError

    def retrieve_files(self, rpath):
        """rpath is the relative path, starting with /sites/..."""
        connection_retries = 0
        while connection_retries < self.MAX_CONNECTION_RETRIES:
            try:
                folder = self.ctx.web.get_folder_by_server_relative_url(rpath)
                file_names = []
                sub_folders = folder.files
                self.ctx.load(sub_folders)
                self.ctx.execute_query(timeout_=self._requests_connection_timeout)

                for s_folder in sub_folders:
                    file_names.append(os.path.join(s_folder.properties["Name"]))

                return file_names

            except Exception as e:
                cprint("[WARN] Problem printing out library contents. Retrying...", "yellow")
                connection_retries += 1
                time.sleep(5)
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))

        cprint("[ERROR] Can't connect to the server.", "red")
        raise ConnectionError

    def download_file(self, download_dir, sharepoint_filepath):
        # 'filepath' needs to be the full relative path to the file, starting with '/sites/...'
        filename = os.path.basename(sharepoint_filepath)
        download_path = os.path.join(download_dir, filename)

        overwrite = False
        if os.path.exists(download_path) and not self.overwrite_all:
            # Check if the file exists on local machine. If it does, ask if we want to overwrite:
            if self.skip_all:
                print("File '{}' exists, skipping.".format(filename))
                return
            while not overwrite:
                answer = input("File '{}' exists. Overwrite? [Y]es, [N]o, [A]ll, [S]kip all: ".format(filename))
                if "N" == answer:
                    print("Not overwriting '{}'".format(filename))
                    return
                elif "Y" == answer:
                    overwrite = True
                elif "A" == answer:
                    print("Overwriting '{}' and all future files.".format(filename))
                    self.overwrite_all = True
                    overwrite = True
                elif "S" == answer:
                    print("Skipping '{}' and all future existing files.".format(filename))
                    self.skip_all = True
                    return
                else:
                    print("Wrong answer, try again.")

        print("Downloading {}...".format(sharepoint_filepath))

        connection_retries = 0
        while connection_retries < self.MAX_CONNECTION_RETRIES:
            try:
                while True:
                    self.segfault = False
                    with open(download_path, "wb") as local_file:
                        self.current_download = download_path
                        self.ctx.web.get_file_by_server_relative_path(sharepoint_filepath).download(
                            local_file
                        ).execute_query_retry(
                            success_callback=self.success_callback, timeout_=self._requests_connection_timeout
                        )
                    if not self.segfault:
                        print("[OK] File {} downloaded properly.".format(os.path.basename(sharepoint_filepath)))
                        self.current_download = None
                        return

            except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
                cprint("[WARN] Connection timeout! Retrying...", "yellow")
                connection_retries += 1
                time.sleep(5)
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))
            except Exception as e:
                cprint("[WARN] Broken connection. Retrying...", "yellow")
                connection_retries += 1
                time.sleep(5)
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))

        cprint("[ERROR] There's a problem downloading the file: {}".format(filename), "red")
        raise ConnectionError

    def make_sharepoint_dir(self, dirname):
        """Assuming "dirname" is relative to self.base_url (i.e. starting with "Shared Documents/...")"""
        connection_retries = 0
        while connection_retries < self.MAX_CONNECTION_RETRIES:
            try:
                folders = self.retrieve_folders(os.path.dirname(dirname))
                if not folders:
                    # If the parent folder doesn't exist, make the parent folder recursively:
                    self.make_sharepoint_dir(os.path.dirname(dirname))
                self.ctx.web.folders.add(dirname)
                self.ctx.execute_query(timeout_=self._requests_connection_timeout)
                return
            except:
                cprint("[WARN] Connection closed. Trying to reconnect...", "yellow")
                connection_retries += 1
                time.sleep(5)
                self.ctx = ClientContext(self.url).with_credentials(UserCredential(self.username, self.password))

        cprint("[ERROR] Can't connect to Sharepoint.", "red")
        raise ConnectionError
