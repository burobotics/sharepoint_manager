#!/usr/bin/env python

import actionlib
import rospy

from sharepoint_downloader import *
from sharepoint_manager_pkg.msg import *

class SharePointServer(object):
    _feedback = SharePointFeedback()
    _result = SharePointResult()

    def __init__(self,name):
        
        self._action_name = name
        self._cancel_name = 'cancel_sharepoint'

        self.sharepoint_parser = SharePointDownloader()

        self._as = actionlib.SimpleActionServer(self._action_name, SharePointAction, execute_cb=self.execute_cb, auto_start=False)
        self._as.start()

        self._cancel_sp_as = actionlib.SimpleActionServer(self._cancel_name,CancelSharePointAction,execute_cb=self.cancel_sharepoint,auto_start=False)
        self._cancel_sp_as.start()

    def cancel_sharepoint(self,goal):
        print("Cancelling sharepoint!")
        self.sharepoint_parser.signal_shutdown()
    
    def execute_cb(self,goal):
        # Here we create the sharepoint downloader! This script should run in the VM
        self.main(goal.url,goal.username,goal.password,goal.local_path,goal.remote_path)

    def main(self,url,username,password,local_path,remote_path):
        '''Download files from SharePoint'''

        result = self.sharepoint_parser.add_user(url,username,password)
        self._result.result = result

        self._as.set_succeeded(result=self._result)

        # Capture keyboard interrupts:
        # signal.signal(signal.SIGINT,self.sharepoint_parser.signal_shutdown)

        '''Full extraction of a session'''
        if result:
            self.sharepoint_parser.main_downloader(local_path,remote_path)

        # self._result.result = 2
        # self._as.set_succeeded(result=self._result)

if __name__ == "__main__":
    
    rospy.init_node("sharepoint_server")

    sharepoint_server = SharePointServer('sharepoint_action')

    print("Waiting for action client...")

    rospy.spin()
