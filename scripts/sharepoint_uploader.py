#!/usr/bin/env python3

import glob
import signal
import subprocess as sp

from sharepoint_manager import *

"""
Edit in utilities/settings.json:
- local_path: The directory where the session was stored, e.g. /media/nuc/reddrive/bags/0.0014
- remote_path: The _relative_ path in Sharepoint where you will store the rosbags, e.g. "Shared Documents/General/HOME_DATA"
    * DON'T start the remote_path with a leading "/" !!

To run:
$ rosrun sharepoint_manager sharepoint_uploader.py
"""


class SharePointUploader(SharePointManager):
    def __init__(self, url, username=None, password=None):
        super().__init__(url, username, password)

    def compress_path(self, filepath):
        """This function splits the file, and stores the parts in a new directory"""

        if os.path.exists(f"{filepath}_parts"):
            cprint(
                f"[WARN] {os.path.basename(filepath)} is already compressed. Remove it manually if you want to re-compress it.",
                "yellow",
            )
            return

        os.mkdir(f"{filepath}_parts")
        cprint(f"Compressing file {os.path.basename(filepath)}...", "cyan")

        self.current_compressed_file = f"{filepath}_parts"

        result = sp.call(
            f'cd "{filepath}_parts" && 7z -v230m a "{os.path.basename(filepath)}.7z" "{filepath}" &> /dev/null',
            executable="bash",
            shell=True,
        )
        if result:
            cprint(
                f"[ERROR] Problem when compressing file '{os.path.basename(filepath)}', with error code: {result}.",
                "red",
            )

            shutil.rmtree(self.current_compressed_file)
            # Better to raise an exception, so that we know for sure that the file wasn't uploaded.
            # Otherwise, if we are uploading a big folder, we might overlook this error:
            # raise CompressionError(os.path.basename(filepath))
        self.current_compressed_file = None

        return result

    def upload_folder(self, local_path, remote_path):
        """local_path: local directory with all the files.
        remote_path: remote directory where we will store all the files."""

        if not self.correct_credentials:
            cprint(
                "[WARN] Credentials not stored. Call .add_user(url,username,password) before",
                "yellow",
            )
            return -1

        # First, make the folder in SharePoint:
        cprint(
            f"Uploading folder '{os.path.basename(local_path)}' to '{os.path.dirname(remote_path)}'...",
            "green",
        )

        # self.make_sharepoint_dir(remote_path)

        # Navigate the folder in the local machine, and upload all the files.
        # If file is too big, need to split it, make a new folder for the parts, and upload everything
        for filepath in glob.glob(f"{local_path}/**", recursive=True):

            # If we want to shut down, don't upload the next file.
            # This should stop all recursive functions as well.
            if self.shutdown:
                return

            relative_path = filepath.split(local_path)[-1][1:]
            filename = os.path.basename(relative_path)

            ### Decide whether to upload or not the videos:
            if filepath[-4:] != ".mp4":
                # Upload only videos
                continue

            if "_Data" in filepath:
                # Skip directories with .png images, which are inside _Data directories
                continue

            if "skip" in filepath or "zip" in filepath:
                # We fully skip already extracted Data, or if it's compressed in a .zip file, or if it's a videos directory
                continue

            if "openpose" in os.path.basename(os.path.dirname(filepath)) and "_parts" not in os.path.basename(
                os.path.dirname(filepath)
            ):
                # Files inside the 'openpose' folders should not be uploaded individually
                continue

            elif (
                "openpose" in os.path.basename(filepath)
                and os.path.isdir(filepath)
                and "_parts" not in os.path.basename(filepath)
            ):
                # It's an 'openpose' folder -- we need to fully compress it and upload it
                cprint("UPLOADING OPENPOSE DIR!", "green")
                result = self.compress_path(filepath)
                if result:
                    # Couldn't compress file, ignore and continue:
                    continue
                relative_path += "_parts"
                folderpath = f"{filepath}_parts"
                # Call upload folder recursively to the new created folder:
                self.upload_folder(folderpath, f"{remote_path}/{relative_path}")

            elif os.path.isfile(filepath):

                remote_folders = self.retrieve_folders(os.path.join(remote_path, os.path.dirname(relative_path)))
                remote_files = self.retrieve_files(os.path.join(remote_path, os.path.dirname(relative_path)))
                if f"{filename}_parts" in remote_folders or filename in remote_files:
                    if self.skip_all:
                        # If we have already compressed the file and uploaded at least one part of it, don't compress it again.
                        cprint(
                            f"[WARN] File {filename} is already uploaded or being uploaded. Skipping.",
                            "yellow",
                        )
                        continue
                    else:
                        cprint(
                            f"[WARN] File {filename} is already uploaded, but I'm overwriting it.",
                            "yellow",
                        )

                file_stats = os.stat(filepath)
                if file_stats.st_size > 1024**2 * 250.0:
                    # Sharepoint doesn't allow uploading files bigger than 250MB through the API.
                    # If file weights more than 250, split and compress.
                    result = self.compress_path(filepath)
                    if result:
                        # Couldn't compress file, ignore and continue:
                        continue
                    relative_path += "_parts"
                    folderpath = f"{filepath}_parts"
                    # Call upload folder recursively to the new created folder:
                    self.upload_folder(folderpath, f"{remote_path}/{relative_path}")
                else:

                    # Upload the file:
                    # self.upload_file(remote_path,local_path)

                    # Upload the file (and make directory):
                    self.make_sharepoint_dir(os.path.join(remote_path, os.path.dirname(relative_path)))
                    print(
                        f"Uploading file {os.path.basename(filepath)} to {os.path.join(remote_path,os.path.dirname(relative_path))}..."
                    )
                    start_time = time.time()
                    self.upload_file(
                        filepath,
                        os.path.join(remote_path, os.path.dirname(relative_path)),
                    )

                    # ## HASH file upload
                    # if '.hash' not in filepath:
                    #     # Create a .hash file and upload it:
                    #     hashfile_name = filepath + '.hash'
                    #     with open(hashfile_name,'w') as f:
                    #         f.write(self.md5(filepath))
                    #     print(f"Uploading HASH file {os.path.basename(hashfile_name)}...")
                    #     self.upload_file(hashfile_name,os.path.join(remote_path,os.path.dirname(relative_path)))
                    #     # Remove the hash file from the local system:
                    #     os.remove(hashfile_name)

                    print(f"File upload took {time.time() - start_time:.2f}s")
                    # After file is uploaded to SharePoint:
                    if ".7z" in os.path.basename(filepath) or ".hash" in os.path.basename(filepath)[-5:]:
                        print(f"Removing {os.path.basename(filepath)} file to save space.")
                        os.remove(filepath)

            if not os.listdir(os.path.dirname(filepath)):
                print("Removing empty directory:", os.path.dirname(filepath))
                os.rmdir(os.path.dirname(filepath))

        return 0


class CompressionError(Exception):
    def __init__(self, file=""):
        self.file = file
        super().__init__()

    def __str__(self):
        return f"File {self.file} couldn't be compressed."


if __name__ == "__main__":

    url = config["sharepoint_settings"]["url"]
    sharepoint_uploader = SharePointUploader(url)
    sharepoint_uploader.add_user()

    # Temporary:
    sharepoint_uploader.skip_all = True

    signal.signal(signal.SIGINT, sharepoint_uploader.signal_shutdown)

    # local_path = '/usr/upload_local_dir'
    local_path = config["upload_settings"]["local_path"]
    remote_path = config["upload_settings"]["remote_path"]
    folder_name = os.path.basename(local_path)
    # If child directory is "bags", replace it for HOME_DATA:
    if folder_name == "bags":
        folder_name = "HOME_DATA"

    result = sharepoint_uploader.upload_folder(local_path, os.path.join(remote_path, folder_name))

    if result == 0:
        cprint(
            "[SUCCESS] All files uploaded successfully. You may unplug the hard drives now.",
            "green",
        )
