#!/usr/bin/env python3

from sharepoint_uploader import *
import glob
import signal
import subprocess as sp

'''
Edit in utilities/settings.json:
- local_path: The directory where the session was stored, e.g. /media/nuc/reddrive/bags/0.0014
- remote_path: The _relative_ path in Sharepoint where you will store the rosbags, e.g. "Shared Documents/General/HOME_DATA"
    * DON'T start the remote_path with a leading "/" !!

To run:
$ rosrun sharepoint_manager sharepoint_uploader.py
'''

if __name__ == "__main__":


    url = config["sharepoint_settings"]["url"]
    sharepoint_uploader = SharePointUploader(url)
    sharepoint_uploader.add_user()

    signal.signal(signal.SIGINT,sharepoint_uploader.signal_shutdown)
    
    # local_path = '/usr/upload_local_dir'
    local_path = config["upload_settings"]["local_path"]
    remote_path = config["upload_settings"]["remote_path"]

    file_stats = os.stat(local_path)
    if file_stats.st_size > 1024**2 * 250.:
        # Sharepoint doesn't allow uploading files bigger than 250MB through the API.
        # If file weights more than 250, split and compress.
        sharepoint_uploader.compress_path(local_path)
        remote_path = os.path.join(remote_path,os.path.basename(local_path) + "_parts")
        folderpath = local_path + "_parts"
        # Call upload folder recursively to the new created folder:
        sharepoint_uploader.upload_folder(folderpath,remote_path)

    else:
        sharepoint_uploader.upload_file(local_path, remote_path)

    cprint("[SUCCESS] File uploaded successfully.","green")
